<?php
session_start();
include_once '_gestionBase.inc.php';

if (isset($_REQUEST))
{
    $codeClient=$_REQUEST['codeClient'];
    $dateDebutReservation=$_REQUEST['dateDebutReservation'];
    $dateFinReservation=$_REQUEST['dateFinReservation'];
    $volumeEstime=$_REQUEST['volumeEstime'];
    $codeVilleMiseDispo=$_REQUEST['codeVilleMiseDispo'];
    $codeVilleRendre=$_REQUEST['codeVilleRendre'];
    $codeReservation = creerReservation($codeClient,$dateDebutReservation,$dateFinReservation,$volumeEstime,$codeVilleMiseDispo,$codeVilleRendre);
    $_SESSION['codeReservation'] = $codeReservation;
}
header ("Location:selectionnerContainerReservation.php");
?>