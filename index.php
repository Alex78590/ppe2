<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <title>login</title>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" href="style.css">
        
    </head>

    <body>
        <?php if (!isset($_SESSION["login"])): ?>
      <div class="signup-form">
        <form action="index.traitement.php" method="post">
            <h1> Connection </h1>
            <input type="text" placeholder="login" name="login" id="login" class="txtb">
            <input type="password" placeholder="mdp" name="mdp" id="mdp" class="txtb">
            <input type="submit" value="Valider" class="signup-btn">
        </form>
      </div>

        <?php else: ?>

        <p><?php echo $_SESSION["login"] ?></p>
        <p><a href="index.traitement.php?logout=1">DECONNEXION</a></p>
        <p><a href="creerReservation.php">RESERVATION</a></p>

        <?php endif; ?>
    </body>
</html>