<?php
include_once '_gestionBase.inc.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="image.css" type="text/css" rel="stylesheet">
    </head>
    
    <body style="background-image:url('moche.jpg');">
        <div id='block'>
            <ul>
                <form action="reservation.traitement.php" method="post">
                <div class="form-group" style="display: block;">
                    <li>
                        <label for="dateDebut" class="required">Début Reservation</label>
                        <input type="date" name="dateDebutReservation" id="dateDebut" required>
                        <br>
                    </li>
                </div>

                <br> 
                <br>

                <div class="form-group" style="display: block;">
                    <li>
                        <label for="first_name" class="required">Fin Reservation</label>
                        <input type="date" name="dateFinReservation" required>
                    </li>
                </div>

                <br>
                <br>

                <div class="form-group" style="display: block;">
                    <li>
                        <label for="first_name" class="required"> Volume éstimé</label>
                        <input type="text" required name="volumeEstime" id="first_name" pattern="[0-9]{0-4}" maxlength="4">
                    </li>
                </div>

                <br>
                <br>

                <div class="form-group" style="display: block;">
                    <li>
                        <label for="salutation" class="required">Ville de départ</label>
                        <select name="codeVilleMiseDispo" data-validate="required" id="salutation">
                        <?php
                        $listeVilles= listeVilles();
                        foreach ($listeVilles as $ville):
                        ?>
                        <option value="<?php echo $ville['codeVille']?>"><?php echo $ville['nomVille']?></option>
                        <?php endforeach;?>
                        </select>
                    </li>
                </div>

                <br>
                <br>

                <div class="form-group" style="display: block;">
                    <li>
                        <label for="salutation" class="required">Ville d'arrivée</label>
                        <select name="codeVilleRendre" data-validate="required" id="salutation">
                        <?php
                        $listeVilles= listeVilles();
                        foreach ($listeVilles as $ville):
                        ?>
                        <option value="<?php echo $ville['codeVille']?>"><?php echo $ville['nomVille']?></option>
                        <?php endforeach;?>
                        </select>
                </div>

                <br>
                <br>

                <input type="submit" value="Valider">
                <a href ="index.php">
            
<a href ="creerReservation.php"></a>


 
                </form>
            </ul>
        </div>
    </body>
</html>

