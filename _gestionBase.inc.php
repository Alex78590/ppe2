<?php
session_start();
function gestionnaireDeConnexion() {
    $pdo = null;
    try {
        $pdo = new PDO(
                'mysql:host=localhost;dbname=ppe2', 'root', 'alexis2000', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    } catch (PDOException $err) {
        $messageErreur = $err->getMessage();
        error_log($messageErreur, 0);
    }
    return $pdo;
}

//authentification
function verification ($login, $mdp)
{
    $compteExistant = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != null)
    {
        $sql = "SELECT count(*) as nb FROM PERSONNE "." WHERE login = :login AND mdp = :mdp";
        $prep = $pdo -> prepare($sql);
        $prep -> bindParam (':login', $login, PDO::PARAM_STR);
        $prep -> bindParam (':mdp', $mdp, PDO::PARAM_STR);
        $prep -> execute();
        $resultat = $prep -> fetch();
        
        if ($resultat["nb"] == 1)
        {
            $compteExistant = true;
        }
        $prep ->closeCursor();
    }
    return $compteExistant;
}

function listeVilles() {
    $lesVilles = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "select * from VILLE order by nomville";
        $pdoStatement = $pdo->query($req);
        $lesVilles = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $lesVilles;
}

function listeContainers() {
    $lesContainers = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "select * from TYPECONTAINER order by typeContainer";
        $pdoStatement = $pdo->query($req);
        $lesContainers = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $lesContainers;
}

function ajouterContainer($codeReservation, $typeContainer, $qteReserver) {
    $ajouterContainer = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $codeReservation = $pdo->quote($codeReservation);
        $typeContainer = $pdo->quote($typeContainer);
        $qteReserver = $pdo->quote($qteReserver);
        $req = "insert into RESERVER (codeReservation, typeContainer, qteReserver) values($codeReservation,$typeContainer,$qteReserver)";
        $pdo->exec($req);
    }
}

function creerReservation($codeClient, $dateDebutReservation, $dateFinReservation, $volumeEstime, $codeVilleMiseDispo, $codeVilleRendre) {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != null) {
        $codeClient = $pdo->quote($codeClient);
        $dateDebutReservation = $pdo->quote($dateDebutReservation);
        $dateFinReservation = $pdo->quote($dateFinReservation);
        $volumeEstime = $pdo->quote($volumeEstime);
        $codeVilleMiseDispo = $pdo->quote($codeVilleMiseDispo);
        $codeVilleRendre = $pdo->quote($codeVilleRendre);

        $req = "insert into RESERVATION (codeClient, dateDebutReservation, dateFinReservation, volumeEstime, codeVilleMiseDispo, codeVilleRendre) 
        values(1,$dateDebutReservation,$dateFinReservation,$volumeEstime,$codeVilleMiseDispo,$codeVilleRendre)";

        $pdo->exec($req);
        $codeReservation = $pdo->lastInsertId() ;
        return $codeReservation;
        
        
    }
}
function recapitulatifReservation($codeReservation)
{
    $recap = array();
    $pdo = gestionnaireDeConnexion();
    
    if ($pdo != NULL)
    {
        $req = "SELECT dateDebutReservation, dateFinReservation, codeVilleMiseDispo, codeVilleRendre, dateReservation, volumeEstime, typeContainer, qteReserver
        FROM RESERVER as R, RESERVATION as RE
        WHERE RE.codeReservation = $codeReservation
        AND R.codeReservation = $codeReservation";

        $pdoStatement = $pdo->query($req);
        $recap = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    
    return $recap;
}
function recupNomVille($codeVille)
{
    $recup = array();
    $pdo = gestionnaireDeConnexion();

    if ($pdo != NULL) {
        $req = "SELECT nomVille
                FROM VILLE
                WHERE codeVille = '$codeVille'";

        $pdoStatement = $pdo->query($req);
        $recup = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        $result = $recup[0]['nomVille'];
    }

    return $result;
}
function recupLibelleTypeContainer($typeContainer)
{
    $recup = array();
    $pdo = gestionnaireDeConnexion();

    if ($pdo != NULL) {
        $req = "SELECT libelleTypeContainer 
                FROM TYPECONTAINER
                WHERE typeContainer = '$typeContainer'";

        $pdoStatement = $pdo->query($req);
        $recup = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        $result = $recup[0]['libelleTypeContainer'];
    }

    return $result;
}


?>

